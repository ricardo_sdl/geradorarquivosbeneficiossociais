<?php

class AqruivoBeneficiosGerador {
    
    const SEPARADOR_COLUNA = "#*#";
    
    private $dataInicioBeneficio;
    private $dataFimBeneficio;
    private $referenciaBeneficio;
    
    private $nomePrograma;
    private $codigoOrgaoResponsavel;
    private $dataPagamento;
    private $codigoIBGEMunicipio;
    
    private $csvFilePath;
    
    
    public function __construct($dataInicioBeneficio, $dataFimBeneficio, $referenciaBeneficio,
        $csvFilePath, $nomePrograma, $codigoOrgaoResponsavel, $dataPagamento, $codigoIBGEMunicipio) {
        
        //informações inexistentes devem ser substituídas por espaço vazio de acordo com o layouy do arquivo
        $this->dataInicioBeneficio = empty($dataInicioBeneficio) ? " " : $dataInicioBeneficio;
        $this->dataFimBeneficio = empty($dataFimBeneficio) ? " " : $dataFimBeneficio;
        $this->referenciaBeneficio = empty($referenciaBeneficio) ? " " : $referenciaBeneficio;
        $this->nomePrograma = empty($nomePrograma) ? " " : $nomePrograma;
        $this->codigoOrgaoResponsavel = empty($codigoOrgaoResponsavel) ? " " : $codigoOrgaoResponsavel;
        $this->dataPagamento = empty($dataPagamento) ? "99999999" : $dataPagamento;
        $this->codigoIBGEMunicipio = empty($codigoIBGEMunicipio) ? " " : $codigoIBGEMunicipio;
        
        $this->csvFilePath = $csvFilePath;
    }
    
    public function converte() {
        $f = fopen($this->csvFilePath, "rb");
        
        if (!$f) {
            return FALSE;
        }
        
        $columns = [
            "CPF" => 1,
            "NOME" => 2,
            "CRÉDITO" => 5];
        
        $convertedLines = [];
        
        while (($line = fgetcsv($f)) !== FALSE) {
            $cpf = $line[$columns['CPF']];
            $cpf = trim(preg_replace('/[^0-9]/', '', $cpf));
            
            $nome = trim($line[$columns['NOME']]);
            
            $credito = $line[$columns['CRÉDITO']];
            $credito = str_replace(",", '.', $credito);
            $credito = number_format($credito, 2, ',', '');
            
            $outputLine = [$cpf, '99999999999999', $nome, $this->nomePrograma, ' ', $this->codigoIBGEMunicipio, ' ', $credito,
                $this->dataInicioBeneficio, $this->dataFimBeneficio, $this->referenciaBeneficio, $this->codigoOrgaoResponsavel,
                $this->dataPagamento
            ];
            
            $convertedLines[] = implode(self::SEPARADOR_COLUNA, $outputLine) . self::SEPARADOR_COLUNA/*pelo layout do arquivo nós temos que ter um separador de coluna no fim*/;
            
        }
        
        return implode(PHP_EOL, $convertedLines) . PHP_EOL;
        
        
        
    }
    
    

}
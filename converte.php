<?php

include './ArquivoBeneficioGerador.php';
include './NomeArquivoBeneficioGerador.php';

$dataInicioBeneficio = $_POST['dataInicioBeneficio'];
$dataFimBeneficio = $_POST['dataFimBeneficio'];
$referenciaBeneficio = $_POST['referenciaBeneficio'];
$dataPagamento = $_POST['dataPagamento'];
$codigoIBGEMunicipio = $_POST['codigoIBGEMunicipio'];
$codigoOrgao = $_POST['codigoOrgao'];
$nomePrograma = $_POST['nomePrograma'];
$siglaPrograma = $_POST['siglaPrograma'];
$isArquivoComplementar = isset($_POST['arquivoComplementar']);

$conversor = new AqruivoBeneficiosGerador($dataInicioBeneficio, $dataFimBeneficio, $referenciaBeneficio,
    $_FILES['arquivoCSVSetransp']['tmp_name'], $nomePrograma, $codigoOrgao, $dataPagamento, $codigoIBGEMunicipio);
$conteudoArquivoConvertido = $conversor->converte();
if ($conteudoArquivoConvertido === FALSE) {
    echo $conteudoArquivoConvertido;
    exit;
}

$tempFile = tempnam(sys_get_temp_dir(), 'BENE');
$f = fopen($tempFile, "wb");
fwrite($f, $conteudoArquivoConvertido);
fclose($f);

$nomeArquivoGerador = new NomeArquivoBeneficioGerador($codigoOrgao, $siglaPrograma, $referenciaBeneficio, date("Ymd"), $isArquivoComplementar);

header('Content-Description: File Transfer');
header("Content-Type: text/csv");
header("Content-disposition: attachment; filename=\"" . $nomeArquivoGerador->geraNomeArquivoBeneficio() . "\"");
header('Content-Length: ' . filesize($tempFile));
readfile($tempFile);




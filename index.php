<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Gerador/Conversor de arquivos para Benefícios Assistenciais/Sociais</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:title" content="">
		<meta property="og:type" content="">
		<meta property="og:description" content="">
		<meta property="og:image" content="">
		<meta property="og:url" content="">
		<link rel="shortcut icon" href="img/favicon.ico" />
		<link rel="apple-touch-icon" href="img/apple_touch_icon.png" />
	</head>
	<body>
		<div class="wrapper">
			<header></header>
			<main>
                <form action="converte.php" method="post" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div>
                        <label for="dataInicioBeneficio">Data Início Benefício(yyyymmdd)</label>
                        <div>
                            <input type="text" id="dataInicioBeneficio" name="dataInicioBeneficio" />
                        </div>
                    </div>
                    
                    <div>
                        <label for="dataFimBeneficio">Data Fim Benefício(yyyymmdd)</label>
                        <div>
                            <input type="text" id="dataFimBeneficio" name="dataFimBeneficio" />
                        </div>
                    </div>
                    
                    <div>
                        <label for="referenciaBeneficio">Referência Benefício(yyyymm)</label>
                        <div>
                            <input type="text" id="referenciaBeneficio" name="referenciaBeneficio" />
                        </div>
                    </div>
					
					<div>
                        <label for="dataPagamento">Data Pagamento(yyyymmdd)</label>
                        <div>
                            <input type="text" id="dataPagamento" name="dataPagamento" />
                        </div>
                    </div>
					
					<div>
                        <label for="codigoIBGEMunicipio">Código Municipio(IBGE)</label>
                        <div>
                            <input type="text" id="codigoIBGEMunicipio" name="codigoIBGEMunicipio" />
                        </div>
                    </div>
                    
                    <div>
                        <label for="codigoOrgao">Código Órgão (Segov = 42)</label>
                        <div>
                            <input type="text" id="codigoOrgao" name="codigoOrgao" value="" />
                        </div>
                    </div>
					
					<div>
                        <label for="nomePrograma">Nome Programa (Passe Livre Estudantil, Cartão Metrobus)</label>
                        <div>
                            <input type="text" id="nomePrograma" name="nomePrograma" value="" />
                        </div>
                    </div>
                    
                    <div>
                        <label for="siglaPrograma">Sigla Programa (PASSELIVRE, CARTAOMETROBUS)</label>
                        <div>
                            <input type="text" id="siglaPrograma" name="siglaPrograma" value="" />
                        </div>
                    </div>
                    
                    <div>
                        <label for="arquivoComplementar">Arquivo Complementar?</label>
                        <div>
                            <input type="checkbox" id="arquivoComplementar" name="arquivoComplementar" value="1" />
                        </div>
                    </div>
                    
                    <div>
                        <label for="arquivoCSVSetransp">Arquivo CSV Origem</label>
                        <div>
                            <input type="file" id="arquivoCSVSetransp" name="arquivoCSVSetransp" />
                        </div>
                    </div>
                    
                    <div>
                        <div>
                            <input type="submit" name="Gerar Arquivo" />
                        </div>
                    </div>
                    
                </form>
            </main>
			<footer></footer>
		</div>
	</body>
</html>
<?php

define("MIN_NUM_PARAMETERS", 4);
define("NOME_PROGRAMA", "Passe Livre Estudantil");
define("CODIGO_ORGAO_RESPONSAVEL", 42);

if (count($argv) < 4) {
    echo "Erro! uso: php processador.php data-inicio-beneficio(yyyymmdd) data-fim-beneficio(yyyymmdd) referencia-beneficio(yyyymm)", PHP_EOL;
    exit(1);
}

$dataInicioBeneficio = $argv[1];
$dataFimBeneficio = $argv[2];
$referenciaBeneficio = $argv[3];



$columns = [
    "CPF" => 1,
    "NOME" => 2,
    "CRÉDITO" => 5];

while (($line = fgetcsv(STDIN)) !== FALSE) {
    $cpf = $line[$columns['CPF']];
    $cpf = trim(preg_replace('/[^0-9]/', '', $cpf));
    
    $nome = trim($line[$columns['NOME']]);
    
    $credito = $line[$columns['CRÉDITO']];
    $credito = str_replace(",", '.', $credito);
    $credito = number_format($credito, 2, ',', '');
    
    $outputLine = [$cpf, '99999999999999', $nome, NOME_PROGRAMA, ' ', ' ', ' ', $credito,
        $dataInicioBeneficio, $dataFimBeneficio, $referenciaBeneficio, CODIGO_ORGAO_RESPONSAVEL];
    
    echo implode("#*#", $outputLine), PHP_EOL;
    
}
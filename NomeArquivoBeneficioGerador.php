<?php

class NomeArquivoBeneficioGerador {
    
    private $codigoOrgao;
    private $siglaPrograma;
    private $referenciaBeneficio;
    private $dataEntregaArquivo;
    private $isArquivoComplementar;
    
    
    public function __construct($codigoOrgao, $siglaPrograma, $referenciaBeneficio, $dataEntregaArquivo, $isArquivoComplementar) {
        
        $this->codigoOrgao = $codigoOrgao;
        $this->siglaPrograma = $siglaPrograma;
        $this->referenciaBeneficio = $referenciaBeneficio;
        $this->dataEntregaArquivo = $dataEntregaArquivo;
        
        $this->isArquivoComplementar = $isArquivoComplementar;
        
    }
    
    public function geraNomeArquivoBeneficio() {
        $parts = [$this->codigoOrgao, $this->siglaPrograma, $this->referenciaBeneficio, $this->dataEntregaArquivo];
        
        if ($this->isArquivoComplementar) {
            $parts[] = 'comp';
        }
        
        return implode("_", $parts) . ".csv";
    }
    
}
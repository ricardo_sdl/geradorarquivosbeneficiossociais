FROM php:7.1-cli

WORKDIR /app

ADD . /app

EXPOSE 80

ENV NAME World

CMD ["php", "-S", "0.0.0.0:80", "-d", "upload_max_filesize=50M"]